package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/mongo"
)

type Api struct {
	engine *gin.Engine
	client *mongo.Database
}

func basePath(route string) string {
	return "/" + route
}

func concatIDParam(p string) string {
	return p + "/:id"
}

func (a *Api) addCreateOne(p string, c *mongo.Collection) {
	a.engine.POST(p, func(c *gin.Context) {
		// TO DO: mongoose query
		c.JSON(http.StatusOK, gin.H{})
	})
}

func (a *Api) addFindOne(p string, c *mongo.Collection) {
	a.engine.GET(concatIDParam(p), func(c *gin.Context) {
		v := c.Request.URL.Query()

		v.Get("id")

		// a.client.
		c.JSON(http.StatusOK, gin.H{})
	})
}

func (a *Api) addFindMany(p string, c *mongo.Collection) {
	a.engine.GET(p, func(c *gin.Context) {
		// TO DO: mongoose query
		c.JSON(http.StatusOK, gin.H{})
	})
}

func (a *Api) addUpdateMany(p string, c *mongo.Collection) {
	a.engine.PUT(p, func(c *gin.Context) {
		// TO DO: mongoose query
		c.JSON(http.StatusOK, gin.H{})
	})
}

func (a *Api) addReplaceOne(p string, c *mongo.Collection) {
	a.engine.PUT(concatIDParam(p), func(c *gin.Context) {
		// TO DO: mongoose query
		c.JSON(http.StatusOK, gin.H{})
	})
}

func (a *Api) addDeleteOne(p string, c *mongo.Collection) {
	a.engine.DELETE(concatIDParam(p), func(c *gin.Context) {
		// TO DO: mongoose query
		c.JSON(http.StatusOK, gin.H{})
	})
}

func (a *Api) addDeleteMany(p string, c *mongo.Collection) {
	a.engine.DELETE(p, func(c *gin.Context) {
		// TO DO: mongoose query
		c.JSON(http.StatusOK, gin.H{})
	})
}

func (a *Api) createRoutes(name string) {
	b := basePath(name)
	c := a.client.Collection(name)

	a.addCreateOne(b, c)
	a.addFindOne(b, c)
	a.addDeleteMany(b, c)
	a.addDeleteOne(b, c)
	a.addFindMany(b, c)
	a.addReplaceOne(b, c)
	a.addUpdateMany(b, c)
}
