FROM golang:1.20-alpine

ENV PORT 8080
ENV GIN_MODE release

WORKDIR /app

COPY go.mod ./
COPY go.sum ./
RUN go mod download

COPY *.go ./

RUN go build -o ./app

EXPOSE 8080

CMD [ "/app" ]