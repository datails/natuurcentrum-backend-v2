package main

import (
	"context"

	"natuurcentrum_backend_v2/utils"
	"time"

	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func setupRouter(uri string) *gin.Engine {
	r := gin.Default()

	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(uri))

	if err != nil {
		panic(err)
	}

	defer client.Disconnect(ctx)

	database := client.Database(utils.EnvVar("MONGO_DB", "test"))

	a := Api{r, database}

	a.createRoutes("user")
	a.createRoutes("category")
	a.createRoutes("client")
	a.createRoutes("order")
	a.createRoutes("product")

	return r
}

func main() {
	uri := utils.EnvVar("MONGO_URI", "mongodb://root:example@localhost:27017")
	r := setupRouter(uri)

	r.Run(utils.EnvVar("SERVER_PORT", ":8080"))
}
