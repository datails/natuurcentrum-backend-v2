# Natuurcentrum Backend V2
Version 2 of the Natuurcentrum Backend, is a direct copy of the [V1 Express server](https://gitlab.com/datails/natuurcentrum-express-backend), but fully written in Go. 

## Development

```bash
docker-compose up -d
go run main.go routes.go
```